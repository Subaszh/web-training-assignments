var students = angular.module('studentdb', []);


//Custom filter for Search
students.filter('searcher', function () {
    return function (students, searchText) {
        var results = [];
        if (searchText === "") {
            return students;
        } else {
            angular.forEach(students, function (student) {
                if (angular.lowercase(student.firstname).indexOf(angular.lowercase(searchText)) >= 0 || angular.lowercase(student.lastname).indexOf(angular.lowercase(searchText)) >= 0) {
                    results.push(student);
                }
            });
            return results;
        }
    };
});
    
   

students.controller('database', function ($http, $scope) {
    $scope.searchText = "";
    
    //Getting Student objects from Json
    $http.get('students.json').success(function (response) {
        $scope.students = response.students;
    });
    
});

