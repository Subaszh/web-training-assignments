var students = angular.module('studentdb', []);

students.controller('database', function ($http, $scope) {
    $scope.name = "firstname";
    $scope.searchText = {firstname : "", lastname: ""};
    
    //Getting Student objects from Json
    $http.get('students.json').success(function (response) {
        $scope.students = response.students;
    });
    
    //Watch over the changes in Seacrh Criteria
    $scope.$watch('name', function (name) {
        if (name === "lastname") {
            $scope.text = $scope.searchText.firstname;
            $scope.searchText = {};
            $scope.searchText.lastname = $scope.text;
        } else {
            $scope.text = $scope.searchText.lastname;
            $scope.searchText = {};
            $scope.searchText.firstname = $scope.text;
        }
    });
});