var app = angular.module("ty", []);

app.directive('checkpwd', function () {
    return {
        require: 'ngModel',
        link: function ($scope, $element, $attr, ngModel) {
            var pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,10}$/,
                myError = false;
            ngModel.$parsers.push(function (value) {
                console.log(value);
                ngModel.$setValidity("myError", pattern.test(value));
                return 
            }
                                 );
        
        }
    };
});

