var app = angular.module("ty", []);
app.controller('pwdvalidation', function ($scope) {
    $scope.$watch("user.password", function (newVal, oldVal) {
        $scope.pcheck = false;
        var pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,10}$/;
        if (!newVal) {
            return;
        }
        $scope.pcheck = pattern.test(newVal);
        console.log(newVal, oldVal);
        console.log($scope.pcheck);
    });
});
   
