var app = angular.module("libapp", ['ngRoute']);

app.config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/technicalbooks', {
        templateUrl: 'techBook.html',
        controller: 'technical'
    }).when('/nontechnicalbooks', {
        templateUrl: 'NontechBook.html',
        controller: 'nontechnical'
    });
}]);

               
//Initiating First controller for technical books
app.controller('technical', function ($scope, $route) {
    $scope.subjects = [
        {
            "id": "0",
            "name": "Embedded Systems",
            "books": [{"name": "PIC16f877A", "stock": "1" }, {"name": "ARM processors", "stock": "0" }, {"name": "RISC OS", "stock": "3" }]
        },
        {
            "id": "2",
            "name": "Web Technology",
            "books": [{"name": "AngularJS", "stock": "2"}, { "name": "HTML5", "stock": "1" }, {"name": "WEBRTC", "stock": "1"}]
        },
        {
            "id": "3",
            "name": "Image Processing",
            "books": [{"name": "OpenCV", "stock": "2"}, { "name": "Raspberry PI board", "stock": "3"}, {"name": "DSP processors", "stock": "2"} ]
        }
    ];
    $scope.selected = $scope.subjects[0];
    $scope.showbooks = false;
  
  //Function to Select subject of books
    $scope.sub_select = function () {
        if ($scope.selected !== null) {
            $scope.showbooks = true;
            
        }
    };
    
    //Function to Add & Remove books in cart
    $scope.checkout = function (val) {
        if (document.getElementById(val).value === "Add to Cart") {
            $scope.selected.books[val].stock -= 1;
            document.getElementById(val).value = "Remove from cart";
        } else {
            $scope.selected.books[val].stock += 1;
            document.getElementById(val).value = "Add to Cart";
        }
    };
});

//Initiating Second controller for Non-technical books
app.controller('nontechnical', function ($scope, $route) {
    $scope.subjects = [
        {
            "id": "1",
            "name": "Fantasy",
            "books": [{"name": "The Hobbit", "stock": "2" }, {"name": "Harry Potter", "stock": "3" }, {"name": "Hunger Games", "stock": "5"}]
        },
        {
            "id": "2",
            "name": "Thriller",
            "books": [{"name": "Sherlock Holmes", "stock": "3" }, {"name": "If Tomorrow Comes", "stock": "4" }, {"name": "Bloodline", "stock": "7" }]
        },
        {
            "id": "3",
            "name": "Romance",
            "books": [{"name": "I too have a Love story", "stock": "3"}, {"name": "Revolution 2020", "stock": "4"}, {"name": "Half Girlfriend", "stock": "6"}]
        }
    ];
    $scope.selected = $scope.subjects[0];
    $scope.showbooks = false;
  
  //Function to Select subject of books
    $scope.sub_select = function () {
        if ($scope.selected !== null) {
            $scope.showbooks = true;
            
        }
    };
    
    //Function to Add & Remove books in cart
    $scope.checkout = function (val) {
        if (document.getElementById((val + 50)).value === "Add to Cart") {
            $scope.selected.books[val].stock -= 1;
            document.getElementById(val + 50).value = "Remove from cart";
        } else {
            $scope.selected.books[val].stock += 1;
            document.getElementById(val + 50).value = "Add to Cart";
        }
    };
});