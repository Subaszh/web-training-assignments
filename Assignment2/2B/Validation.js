angular.module("validity", [] )
    .controller("validation", function ($scope) {
        $scope.valid_Status = false;
        $scope.mails = [];
        var i = 0;
        $scope.validate = function () {
            var mformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
            
            //Validating the Entered Mail-id For every change in the Input
            if(mformat.test ($scope.mail)) {                
                $scope.valid_Status = true;
            } else {
                $scope.valid_Status = false;
                $scope.submit_status = false;
            }
        }
        
          //Submitting mail Id to the Table
        $scope.submit = function () {
            $scope.submit_status = true;
            $scope.mails.push($scope.mail);//Push Current mail Id to the existing array
        }
   });