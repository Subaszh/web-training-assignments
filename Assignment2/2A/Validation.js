var app = angular.module("validity", []);
app.controller("validation", function ($scope) {
    $scope.valid_Status = false;

    //Validating the Entered Mail-id For every change in the Input
    $scope.validate = function () {
        var mformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (mformat.test($scope.mail)) {
            $scope.valid_Status = true;
        } else {
            $scope.valid_Status = false;
            $scope.submit_status = false;
        }
    };

    //Setting display or Failure Status on Button Click
    $scope.display = function () {
        if ($scope.valid_Status) {
            $scope.submit_status = true;
        }
    };
});