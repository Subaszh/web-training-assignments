var app = angular.module("libapp", []);

//Initialising First Controller for Technical Books
app.controller('technical', function ($scope, $rootScope) {
    $scope.subjects = [
        {
            "id": "1",
            "name": "Embedded Systems",
            "books": ["PIC16f877A", "ARM processors", "RISC OS"]
        },
        {
            "id": "2",
            "name": "Web Technology",
            "books": [  "AngularJS", "HTML5", "WEBRTC"]
        },
        {
            "id": "3",
            "name": "Image Processing",
            "books": ["OpenCV", "Raspberry PI board", "DSP processors" ]
        }
    ];
    $scope.selected = $scope.subjects[0];
    $scope.showbooks = false;
    
    //Function to select subject of the books
    $scope.sub_select = function () {
        if ($scope.selected !== null) {
            $scope.showbooks = true;
            $scope.book_selected = $scope.selected.books[0];
        }
    };
    
    //Funtion to reset the checkout
    $scope.reset = function () {
        $rootScope.checkedout = false;
    };
    
    //Function to checkout a book
    $scope.checkout = function () {
        $rootScope.checkedout = true;
    };
});

//Initialising Second Controller for Non-Technical books
app.controller('nontechnical', function ($scope, $rootScope) {
    $scope.subjects = [
        {
            "id": "1",
            "name": "Fantasy",
            "books": [ "The Hobbit", "Harry Potter", "Hunger Games"]
        },
        {
            "id": "2",
            "name": "Thriller",
            "books": [ "Sherlock Holmes", "If Tomorrow Comes", "Bloodline"]
        },
        {
            "id": "3",
            "name": "Romance",
            "books": [ "I too have a Love story", "Revolution 2020", "Half Girlfriend"]
        }
    ];
    $scope.selected = $scope.subjects[0];
    $scope.showbooks = false;
    
    //Functiion to selec subject of the books
    $scope.sub_select = function () {
        if ($scope.selected !== null) {
            $scope.showbooks = true;
            $scope.book_selected = $scope.selected.books[0];
        }
    };
    
    //Funtion to reset the checkout
    $scope.reset = function () {
        $rootScope.checkedout = false;
    };
    
    //Function to checkout a book
    $scope.checkout = function () {
        $rootScope.checkedout = true;
    };
});