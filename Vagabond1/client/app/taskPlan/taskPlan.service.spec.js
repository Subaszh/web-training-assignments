'use strict';

describe('Service: taskPlan', function () {

  // load the service's module
  beforeEach(module('vagabond1App'));

  // instantiate service
  var taskPlan;
  beforeEach(inject(function (_taskPlan_) {
    taskPlan = _taskPlan_;
  }));

  it('should do something', function () {
    expect(!!taskPlan).toBe(true);
  });

});
