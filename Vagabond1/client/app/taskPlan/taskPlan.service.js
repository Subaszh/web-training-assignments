'use strict';

angular.module('vagabond1App')
  .service('taskPlan', function () {
    // AngularJS will instantiate a singleton by calling "new" on this function
    this.tasks = [];
  });
