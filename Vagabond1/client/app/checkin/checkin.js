'use strict';

angular.module('vagabond1App')
  .config(function ($routeProvider) {
    $routeProvider
      .when('/checkin', {
        templateUrl: 'app/checkin/checkin.html',
        controller: 'CheckinCtrl'
      });
  });
