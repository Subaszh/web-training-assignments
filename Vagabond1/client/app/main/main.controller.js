'use strict';

angular.module('vagabond1App')
    .controller('MainCtrl', function ($scope, $http, taskPlan) {
        $scope.add = true;
        $scope.initialize = {};
        $scope.check = [];
        $scope.initialize = function (lat, lng, zoomValue) {
            var mapCenter = new google.maps.LatLng(lat, lng);
            var mapProp = {
                center: mapCenter,
                zoom: zoomValue,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map-canvas"), mapProp);
            var marker = new google.maps.Marker ({
                position: mapCenter
            });
            marker.setMap(map);
        };
            
      
        $scope.search = function () {
            
            $http.get("https://api.foursquare.com/v2/venues/explore?near=" + $scope.searchBox + "&oauth_token=ZBFN0DJ5RCPWIV0WO2G4TRQGDX1U4OA4XGTOTKITZUWNXXA4&v=20150603")
            .then(function (res) {
                    console.log(res.data.response.groups[0].items[0].venue);
                    $scope.initialize(res.data.response.geocode.center.lat, res.data.response.geocode.center.lng, 17);
                    $scope.searchBox = res.data.response.geocode.displayString;
                    $scope.items = res.data.response.groups[0].items;
                }, function (err) {
                    console.log(err.status);
             });    
        };
    
        $scope.add2Plan = function (item) {
            $scope.check[item] = !$scope.check[item];
            (!$scope.check[item]) ? taskPlan.tasks.push($scope.items[item]) :  taskPlan.tasks.splice(taskPlan.tasks.indexOf($scope.items[item]), 1);
            (taskPlan.tasks.length !== 0)? $scope.save = true : $scope.save = false;
            console.log(taskPlan.tasks);
        };
    });
