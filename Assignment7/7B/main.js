var avg = angular.module('runningavg', []);
avg.controller('uiController', function ($scope, $rootScope) {
    'use strict';
    $scope.elements = [];
    $scope.results = [];
    $scope.include = function (value) {
        $scope.val = "";
        
        //Pushing Input to th Array
        if (angular.isNumber(value)) {
            $scope.elements.push(value);
        } else {
            $scope.val = "";
        }
    };
    
        
    //Validating the Span Value with Array Length
    $scope.validate = function () {
        if ($scope.span > $scope.elements.length) {
            $scope.valid = true;
        } else {
            $scope.val = "";
            $scope.valid = false;
        }
    };
    
    $scope.runAvg = function () {
        $scope.results = [];
        $scope.$broadcast('input', {inputs: $scope.elements, span: $scope.span});
    };
    $scope.$on('output', function (event, output) {
        $scope.results.push(output.result);
    });
});

avg.controller('avgController', function ($scope, $q, $rootScope) {
    var elements = [], span, i;
    $scope.$on('input', function (event, input) {
        elements = input.inputs;
        span =  $scope.span;
        //Calculating running average
        for (i = 0; i < (elements.length - span + 1); i += 1) {
            promise = calcAvg(i);
            promise.then(function (val) {
                $scope.$emit('output', {result: val});
            }, function (err) {
                console.log(err);
            });
        }
    });

    //Asynchronous Function
    var calcAvg = function (period) {
        var deferred = $q.defer(), j = 0, total = 0, result;
        setTimeout(function () {
            for (j = period; j < (period + span); j += 1) {
                total = total + elements[j];
                console.log(total);
            }
            result = total / (span);
            deferred.resolve(result);
        }, 10);
        return deferred.promise;
    };
    
 
    
});
                    
                
        
   
