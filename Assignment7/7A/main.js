var avg = angular.module('runningavg', []);
avg.controller('average',function ($scope, $q, $timeout) {
    'use strict';
    $scope.elements = [];
    $scope.include = function (value) {
        $scope.val = "";
        
        //Pushing Input to th Array
        if (angular.isNumber(value)) {
            $scope.elements.push(value);
        } else {
            $scope.val = "";
        }
    };
    
    //Asynchronous Function
    var calcAvg = function (period) {
        var deferred = $q.defer(), j = 0, total = 0, result;
        setTimeout(function () {
            for (j = period; j < (period + $scope.span); j += 1) {
                total = total + $scope.elements[j];
                console.log(total);
            }
            result = total / ($scope.span);
            deferred.resolve(result);
        }, 1000);
        return deferred.promise;
    };
    
    
    //Validating the Span Value with Array Length
    $scope.validate = function () {
        if ($scope.span > $scope.elements.length) {
            $scope.valid = true;
        } else {
            $scope.val = "";
            $scope.valid = false;
        }
    };
    
    //Calculating running average
    $scope.runAvg = function () {
        var i = 0, promise;
        $scope.results = [];
        for (i = 0; i < ($scope.elements.length - $scope.span + 1); i += 1) {
            promise = calcAvg(i);
            promise.then(function (val) {
                $scope.results.push(val);
            }, function (err) {
                console.log(err);
            });
        }
    };
});
                    
                
        
   
